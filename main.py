import discord
import os
import random
from datetime import datetime, time
from discord.ext import commands, tasks
from discord.utils import get
from discord import Embed
from keep_alive import keep_alive

TIME = time(hour=19, minute=37)

REACTIONS_PATH = "/home/runner/XYZ/Reactions/"
PICTURES_PATH = "/home/runner/XYZ/Pictures/"

CHANNEL_ID = int(os.getenv("CHANNEL_ID"))
CHANNEL_TWO_ID = int(os.getenv("CHANNEL_TWO_ID"))

SELECTED_CHANNELS = [CHANNEL_ID, CHANNEL_TWO_ID]

quotes = [
  "Lorem ipsum"
]

client = discord.Client(intents=discord.Intents.all(),
                        activity=discord.Activity(
                          type=discord.ActivityType.playing,
                          name="lorem ipsum"))

emotes = [
  " <:lorem_ipsum:1112817432562774199>",
]

react_emotes = {
  "nice": ["🇳", "🇮", "🇨", "🇪"],
}

def choose_image(path):
  files = os.listdir(path)
  picture = random.choice(files)
  return picture


@client.event
async def on_ready():
  print("we have logged in")
  counter.start()
  time = str(datetime.now())[11:16]
  print(time)

  
@client.event
async def on_message(message):
  if client.user.id != message.author.id:
    if len(message.attachments) > 0 and message.channel.id in SELECTED_CHANNELS:
      channel = client.get_channel(CHANNEL_ID)
      coin_flip = random.randint(0,1)
      if coin_flip:
        reaction = random.choice(list(react_emotes))
        for emoji in react_emotes[reaction]:
          await message.add_reaction(f"{emoji}")
      else:
        await channel.send("",file=discord.File(REACTIONS_PATH +
                                               choose_image(REACTIONS_PATH)))


@tasks.loop(time=TIME)
async def counter():
  file = open("/home/runner/counter.txt", "r")
  counter = int(file.read())
  file.close()
  counter += 1
  file = open("/home/runner/counter.txt", "w")
  file.write(str(counter))
  file.close()
  channel = client.get_channel(CHANNEL_ID)
  message = random.choice(quotes) + "\n" + str(
    counter) + " lorem ipsum!"
  await channel.send(f"{message}",
                     file=discord.File(PICTURES_PATH +
                                               choose_image(PICTURES_PATH)))


keep_alive()
try:
  client.run(os.getenv('TOKEN'))
except:
  print("Rebooting...")
  __import__('os').system('kill 1')
