# Discord Bot
This is an exemplar discord bot that can be hosted using right tools.

## Setup
To host this bot you have to do three things:
1. Check all boxes under Privileg Gateway Intents
in [Discord](https://discord.com/developers/) API site
under BOT tab.
2. Upload your bot on [Replit](https://replit.com/~) and start it.
3. Keep your bot up by monitoring it using [UptimeRobot](https://uptimerobot.com/).

### Do tasks at certain hours
To make bot do things at certain hours there is a `@task.loop` decorator.
You can pass to it a time you specified and then in `on_ready()` function
start the task with `task_function_name.start()`.

### Send a random image from a folder
To send an image via message from a directionary you have to give a full path
to the image to a *discord.File()*. To randomly choose an image use the *choose_image*
function and give it a path to the images directory.

### Send a chain of reactions
To send more than one reaction you can use a dict to hold arrays of emojis that you can
iterate over and then send them one by one.
